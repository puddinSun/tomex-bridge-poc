import "./App.css";
import { ethers, utils } from "ethers";
import { useState } from "react";
import LockABI from "./abis/Lock.json";

function App() {
  const [address, setAdress] = useState("");
  const [provider, setProvider] = useState(null);
  const [txResult, setTxResult] = useState(null);
  const [walletPK, setWalletPK] = useState(null);

  const connectWallet = async () => {
    const provider = new ethers.providers.JsonRpcProvider(
      "http://localhost:8545"
    );
    const signer = provider.getSigner();
    const address = await signer.getAddress();
    console.log("Signer address ...", address);
    setAdress(address);
    setProvider(provider);

    const blockNumber = await provider.getBlockNumber();
    const balance = await provider.getBalance(address);
    console.log(blockNumber);
    console.log(utils.formatEther(balance));
  };

  const lockETH = async () => {
    const LockContract = new ethers.Contract(
      "0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512",
      LockABI.abi,
      provider
    );

    const signer = await provider.getSigner();
    const contractWithSigner = LockContract.connect(signer);

    contractWithSigner.withdraw().then((tx) => {
      console.log(tx.blockHash);
      setTxResult(tx);
    });

    console.log("address for posting: ", address);

    contractWithSigner.on("Withdrawal", (address, timestamp) => {
      let transferEvent = {
        from: address,
        timestamp: timestamp,
      };
      console.log("After withdraw - ", JSON.stringify(transferEvent, null, 4));
    });
  };

  const createWallet = () => {
    const newWallet = ethers.Wallet.createRandom();
    console.log(newWallet);
    setWalletPK(newWallet.privateKey);
  };

  return (
    <div className="App">
      {txResult && <div>{txResult.blockHash}</div>}

      {address && (
        <div>
          <p>Connected to Address: {address}</p>
          <button style={{ margin: 10 }} onClick={lockETH}>
            Withdraw
          </button>
        </div>
      )}

      {!address && (
        <div>
          <p>Connect to wallet</p>
          <button onClick={connectWallet}>Connect</button>
        </div>
      )}

      <div>
        Create a random wallet
        <button onClick={createWallet}>create</button>
        <p>{walletPK && "Wallet PK: " + walletPK}</p>
      </div>
    </div>
  );
}

export default App;
