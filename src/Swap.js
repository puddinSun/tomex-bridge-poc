import "./App.css";

import { BigNumber, ethers, utils } from "ethers";

import RouterJSON from "./abis/IStargateRouter.json";
import { useState } from "react";

function Swap() {
  const [address, setAdress] = useState("");
  const [swapFee, setSwapFee] = useState(0);
  const [provider, setProvider] = useState(null);

  const connectWallet = async () => {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const network = await provider.getNetwork();
    console.log("chainId", network.chainId);
    if (network.chainId !== 97) {
      _requestSwitchNetwork(97);
    }
    const signer = provider.getSigner();
    const address = await signer.getAddress();
    console.log("Signer address ...", address);
    setAdress(address);
    setProvider(provider);

    const blockNumber = await provider.getBlockNumber();
    const balance = await provider.getBalance(address);
    console.log(blockNumber);
    console.log(utils.formatEther(balance));
  };

  const _requestSwitchNetwork = async (chainId) => {
    try {
      const chainIdHex = "0x" + chainId;
      console.log("chainIdHex ___ " + chainIdHex);
      await window.ethereum.request({
        method: "wallet_switchEthereumChain",
        params: [{ chainId: chainIdHex }],
      });
      window.location.reload(false);
    } catch (ex) {
      // https://docs.metamask.io/guide/rpc-api.html#other-rpc-methods
      // This error code indicates that the chain has not been added to MetaMask.
      // 4001 error means user has denied the request
      // If the error code is not 4001, then we need to add the network
      if (ex.code !== 4001) {
        await _requestAddNetwork("0x" + chainId.toString(16));
      } else {
        // eslint-disable-next-line no-console
        console.error("swithNetwork error ___ ", ex);
      }
    }
  };

  const _requestAddNetwork = async (chainIdHex) => {
    try {
      await window.ethereum.request({
        method: "wallet_addEthereumChain",
        params: [
          {
            chainId: chainIdHex,
            chainName: "BSC test network",
            rpcUrls: ["https://data-seed-prebsc-2-s3.binance.org:8545"],
            blockExplorerUrls: ["https://testnet.bscscan.com"],
            nativeCurrency: {
              symbol: "tBNB",
              decimals: 18,
            },
          },
        ],
      });
      window.location.reload(false);
    } catch (err) {
      console.log("addNetwork error ___", err);
    }
  };

  const getSwapFee = async () => {
    const signer = provider.getSigner();
    const address = await signer.getAddress();

    const routerContract = new ethers.Contract(
      "0xbB0f1be1E9CE9cB27EA5b0c3a85B7cc3381d8176",
      RouterJSON.abi,
      provider
    );

    const contractWithSigner = routerContract.connect(signer);
    const abiCoder = utils.defaultAbiCoder;
    console.log(abiCoder.encode(["address"], [address]));

    const fees = await contractWithSigner.quoteLayerZeroFee(
      10143,
      1,
      abiCoder.encode(["address"], [address]),
      "0x", // payload, using abi.encode()
      {
        dstGasForCall: 0, // extra gas, if calling smart contract,
        dstNativeAmount: 0, // amount of dust dropped in destination wallet
        dstNativeAddr: address, // destination wallet for dust
      }
    );
    setSwapFee(fees[0].toString());
  };

  const swapNow = async () => {
    const signer = provider.getSigner();
    const address = await signer.getAddress();

    const routerContract = new ethers.Contract(
      "0xbB0f1be1E9CE9cB27EA5b0c3a85B7cc3381d8176", // BSC Test
      RouterJSON.abi,
      provider
    );

    const contractWithSigner = routerContract.connect(signer);
    const abiCoder = utils.defaultAbiCoder;

    console.log("Log with GAS FEE ____ " + swapFee);
    // const amout2Swap = utils.parseUnits("0.2", "ether");
    // console.log("Parsing UNIT >>> ", amout2Swap.toString());

    try {
      const swapTx = await contractWithSigner.swap(
        10143, // arbitrum test net
        2,
        2,
        address, // return
        "100",
        0,
        { dstGasForCall: 0, dstNativeAmount: 0, dstNativeAddr: "0x" },
        abiCoder.encode(["address"], [address]),
        "0x",
        { value: BigNumber.from(swapFee) }
      );

      console.log(swapTx);
    } catch (err) {
      console.log(err);
      alert("Stargate: TRANSFER_FROM_FAILED >> " + err);
    }
  };

  const createWallet = () => {
    const newWallet = ethers.Wallet.createRandom();
    console.log(newWallet);

    const walletFwd = {
      address: newWallet.address,
      publicKey: newWallet.publicKey,
      privateKey: newWallet.privateKey,
      passPhrase: newWallet.mnemonic.phrase,
    };

    console.log(JSON.stringify(walletFwd));
    localStorage.setItem("walletFwd", JSON.stringify(walletFwd));
  };

  return (
    <div className="App">
      {address && (
        <>
          <div>
            Let's swap
            {address && <div>Connected with account - {address}</div>}
            <button style={{ marginTop: 20 }} onClick={getSwapFee}>
              Get swap fee
            </button>
            <p>Swap fee: {swapFee}</p>
          </div>

          <div>
            <button onClick={swapNow}>Swap now</button>
          </div>

          <div style={{ margin: 10 }}>
            <button onClick={createWallet}>Generate new wallet</button>
          </div>
        </>
      )}

      {!address && <button onClick={connectWallet}>Connect</button>}
    </div>
  );
}

export default Swap;
