import { CounterStore } from "../domain/CounterStore";

type GetCounterStore = Pick<CounterStore, "loadInitialCounter">;

const getCounterUseCase = (counter: GetCounterStore) => {
  counter.loadInitialCounter();
};

export { getCounterUseCase };
