import { increment } from "../domain/CounterModel";
import type { UpdateCounterStore } from "./UpdateCounterUserCase";
import { updateCounterUseCase } from "./UpdateCounterUserCase";

const incrementCounterUseCase = (store: UpdateCounterStore) => {
  return updateCounterUseCase(store, increment);
};

export { incrementCounterUseCase };
