import { decrement } from "../domain/CounterModel";
import type { UpdateCounterStore } from "./UpdateCounterUserCase";
import { updateCounterUseCase } from "./UpdateCounterUserCase";

const decrementCounterUseCase = (store: UpdateCounterStore) => {
  return updateCounterUseCase(store, decrement);
};

export { decrementCounterUseCase };
