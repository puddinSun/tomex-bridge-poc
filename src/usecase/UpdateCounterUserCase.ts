import { CounterEntity } from "../domain/CounterEntity";
import { CounterStore } from "../domain/CounterStore";

type UpdateCounterStore = Pick<
  CounterStore,
  "counter" | "setCounter" | "updateCounter"
>;

const updateCounterUseCase = (
  store: UpdateCounterStore,
  action: (store: CounterEntity) => CounterEntity
) => {
  const updatedCounter = store.counter ? action(store.counter) : store.counter;

  if (updatedCounter) {
    store.setCounter(updatedCounter);
    store.updateCounter(updatedCounter);
  }
};

export { updateCounterUseCase };
export type { UpdateCounterStore };
