import { CounterEntity } from "./CounterEntity";

const create = (initValue: CounterEntity["value"]) => ({ value: initValue });

const decrement = (counter: CounterEntity) => ({
  value: Math.max(counter.value - 1, 0),
});

const increment = (counter: CounterEntity) => ({ value: counter.value + 1 });

export { create, decrement, increment };
