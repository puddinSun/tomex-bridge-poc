import { CounterEntity } from "./CounterEntity";

export interface CounterStore {
  // states
  counter: CounterEntity | undefined;
  isLoading: boolean;
  isUpdating: boolean;

  // actions
  loadInitialCounter(): Promise<CounterEntity>;
  setCounter(counter: CounterEntity): void;
  updateCounter(counter: CounterEntity): Promise<CounterEntity | undefined>;
}
